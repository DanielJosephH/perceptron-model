import numpy as np 

class Perceptron:

    def __init__(self, num_params, learning_rate, positive_label):
        self.learning_rate = learning_rate
        self.positive_label = positive_label
        self.weights = np.random.rand(1, num_params + 1) # contains bias and input variable weights
        self.errors = []

    def recall(self, instance):
        sum = (self.weights * instance).sum()
        return self.activation(sum)

    def activation(self, input):
        if input > 0:
            return 1
        else:
            return 0

    def get_target(self, label):
        if label == self.positive_label:
            return 1 
        else:
            return 0

    def train(self, data, max_iterations):
        iteration = 0
        while iteration < max_iterations:
            iteration += 1

            # shuffle data to prevent inabaility to converge due to data changing down dataset 
            # this ensures the chosen datapoint is i.i.d for stochastic gradient descent
            np.random.shuffle(data) 
            
            for line in data:
                instance = np.hstack((np.array([1]), line[:-1])).astype(np.float)
            
                output = self.recall(instance)
                target = self.get_target(line[-1])

                # update the weights
                # uses stochastic gradient descent
                self.weights = self.weights - self.learning_rate * (output - target) * instance

            if iteration % 5 == 0:
                errors = self.evaluate(data)
                self.errors.append(errors)

                if errors == 0:
                    print("0 errors, stopped training")
                    break

        if iteration >= max_iterations:  
            print("Max iterations reached, stopped training")      

    def evaluate(self, data):
        errors = 0
        for line in data:
                instance = np.hstack((np.array([1]), line[:-1])).astype(np.float)
            
                output = self.recall(instance)
                target = self.get_target(line[-1])

                if not (output == target):
                    errors += 1 
        return errors


