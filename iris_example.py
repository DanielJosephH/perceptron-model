import numpy as np 
import matplotlib as plt

from perceptron import *

# As there are 3 possible classes then they are not linearly seperable, and hence cannot be classified using a single perceptron. 
# However, by using 3 perceptrons and classifying as class vs non-class for each class we can successfully classify most examples.
# This shows the limitations of perceptrons and possible approaches to overcome them. Using a multi-layer neural network will 
# classify this dataset much more accurately.

# create 3 neurons for classifying each label. Classify as positive label vs all other labels. 
# E.g iris-virginica vs non-iris-virginica (iris-versicolor & iris-setosa)
p1 = Perceptron(4, 0.01, "Iris-setosa")
p2 = Perceptron(4, 0.01, "Iris-versicolor")
p3 = Perceptron(4, 0.01, "Iris-virginica")

    
# load training data
filename = 'iris.data'
filepath = "./datasets/{}".format(filename)
data = None
try:
    raw_data = open(filepath, 'rt')
    data = np.loadtxt(raw_data, delimiter=",", dtype=str)
    np.random.shuffle(data)
except Exception as error:
    print("ERROR: failed to load 'iris.data' due to, %s" % error)

# train perceptron on data
p1.train(data, 1000)
p2.train(data, 1000)
p3.train(data, 1000)


# evaluate model on test data (same as training data in this case as this is just an example)
errors = 0
for line in data:
    instance = np.hstack((np.array([1]), line[:-1])).astype(np.float)

    output = [p1.recall(instance), p2.recall(instance), p3.recall(instance)]

    # target should be a single active perceptron, if multiple are active then the model is undecided
    if line[-1] == "Iris-setosa":
        target = [1,0,0]
    elif line[-1] == "Iris-versicolor":
        target = [0,1,0]
    elif line[-1] == "Iris-virginica":
        target = [0,0,1]
    else:
        print("ERROR: out of bounds class label, %s" % line[-1])
        exit(-1)

    if not (output == target):
        errors += 1
print("Errors: %d" % errors)

# plot errors